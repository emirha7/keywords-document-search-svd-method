import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Scanner;
import org.jblas.DoubleMatrix;
import org.jblas.Singular;
import org.jblas.Solve;

public class FrequencyMatrix {
	//globalen slovar v katerem belezimo besede in njihove frekvence
	static LinkedHashMap<String, Integer> besede = new LinkedHashMap<String, Integer>();
	//ArrayList vseh dokumentov
	public ArrayList<String> dokumenti = new ArrayList<String>();
	//matrika A (vrstice->besede, stolpci ->dokumenti)
	public DoubleMatrix modifyMatrix;
	
	//popravljanje vrednosti originalne matrike, ki vsebuje
	//le frekvence (racunanje globalne in lokalne mere)
	public DoubleMatrix approx(DoubleMatrix m, boolean addDoc) {
		//indeks pisanja elementa v vrstico
		int c = 0;
		//podano matriko pretvorimo v 2d polje realnih stevil
		double[][] frekMat = m.toArray2();
		//iteracija skozi vse besede
		for (String s : besede.keySet()) {
			//dobimo globalno frekvenco
			double gij = besede.get(s);

			double pij;
			double Gi = 1;
			double n = m.columns;
			//racunanje globalne frekvence
			//ce nov dokument ni dodan potem izraunamo vse vrednosti
			//sicer popravljamo vrednosti le frekvenco dodanih dokumentov
			for (int j = (!addDoc)?0:m.columns-1; j < frekMat[c].length; j++) {
				if (frekMat[c][j] != 0) {
					pij = frekMat[c][j] / gij;
					Gi -= (pij *log2(pij) / log2(n));
				}
			}
			//globalna frekvenca * lokalna frekvenca
			for (int j = 0; j < frekMat[c].length; j++) {
				frekMat[c][j] = log2(frekMat[c][j] + 1) * Gi;
			}
			//nasljednic pisemo na nov indeks tabele
			c++;
		}
		//vrnemo dobljeni rezultat
		return new DoubleMatrix(frekMat);
	}

	//metoda vrne vektor poizvedbe
	public DoubleMatrix calcQVector(String str) {
		//velikost vektorja -> toliko kolikor je besed
		double[][] queryVector = new double[1][besede.size()];
		//uporabnisko poizvedbo razbijemo po presledkih
		String[] arr = str.split(" ");
		//indeks kamor pisemo v poizvedovalni vektor
		int i = 0;
		//iteriramo skozi vse besedo
		for (String key : besede.keySet()) {
			for (int j = 0; j < arr.length; j++) {
				if (key.equals(arr[j])) {
					//ce najdemo i-to besedo zabelezimo enico
					queryVector[0][i] = 1;
				}
			}
			//premaknemo se z indeksom naprej, ker je beseda obdelana
			i++;
		}

		return new DoubleMatrix(queryVector);
	}

	//metoda vrne dvojiski logaritem stevila x
	public double log2(double x) {
		return Math.log(x)/Math.log(2);
	}
	
	//metoda na podlagi poizvedovalnega niza vrne vrne najblizji dokument temu
	public ArrayList<String> search(DoubleMatrix m, String sVector, int k, DoubleMatrix[] mat, double cos) {
		//izracun poizvedovalnega vektorja
		DoubleMatrix Q = calcQVector(sVector);
		//s pomocjo koncnega svdja izracuna inverz
		double[][] novaMatrikaS = new double[k][k];
		//rezultat poizvedbe bodo dokumenti, ArrayList potrebujemo, da si te belezimo
		ArrayList<String> docs = new ArrayList<String>();
		//inverz diagonalne matrike izracunamo tako da delimo 1 delimo z elementom
		for (int j = 0; j < k; j++) {
			novaMatrikaS[j][j] = 1 / mat[1].get(j, j);
		}

		//preskimao vektor v nas prostor s pomocjo svd razcepa
		DoubleMatrix qr = Q.mmul(mat[0]).mmul(new DoubleMatrix(novaMatrikaS));
		DoubleMatrix matrikaDok = qr.mmul(mat[2]);
		//nroma vektorja za potrebe kosinusnega izreka
		double normaq = qr.norm2();

		//iteracija skozi matriko V
		for (int j = 0; j < matrikaDok.columns; j++) {
			//izracun kota po kosinusnem izreku med dokuemnti in poizvedovalnim vektorjem
			DoubleMatrix sub = matrikaDok.get(new int[] { 0, 0 }, new int[] { j });
			double dubl = sub.get(0, 0);
			double norma1 = mat[2].get(row(mat[2].rows), new int[] { j }).norm2();
			//ce je vrednost cosinusa vecja od podanee vrednosti jo dodamo v rezulat 
			if (dubl / (normaq * norma1) >= cos) {
				//System.out.println("dokument " + dokumenti.get(j) + " je povezan cos " + dubl / (normaq * norma1));
				docs.add(dokumenti.get(j));
			}
		}
		//vrnemo rezulat
		return docs;
	}
	//metoda izracuna svd razcep
	public DoubleMatrix[] calcSVD(DoubleMatrix m) {
		return Singular.sparseSVD(m);
	}

	//metoda belezi frekvence besed in vrne matriko teh
	public DoubleMatrix calcMatrix(File folder) throws FileNotFoundException {
		//arraylist slovarjev za posamezen dokument
		ArrayList<LinkedHashMap<String, Integer>> sezDoc = new ArrayList<LinkedHashMap<String, Integer>>();

		//iteracija skozi vse datoteke v podanem direktoriju
		for (File file : folder.listFiles()) {
			//kreiramo scanner
			Scanner sc = new Scanner(file);
			//pripravimo slovar za vsako datoteko
			LinkedHashMap<String, Integer> map = new LinkedHashMap<String, Integer>();

			//iteriramo skozi vse besede v datoteki
			while (sc.hasNext()) {
				//pretovrimo besedo v male crke in izlocimo vse znake ki niso crke
				String s = sc.next().toLowerCase().replaceAll("[^a-zA-Z]", "");
				//ce ostane prazen niz si ga ne zelimo dodati v matriko A
				if (s.length() >= 1) {
					//priprava slovarja za izracun globalnih mer
					//ce beseda ze obstaja v slovarju ji frekvenco povecamo za 1
					if (besede.containsKey(s)) {
						besede.put(s, besede.get(s) + 1);
					//sicer ji frekvenco nastavimo na 1, saj gre za prvo pojavitev te besede
					} else {
						besede.put(s, 1);
					}
					//priprava slovarja za vektor, ki bo predstavljal ta dokument
					//ce se je beseda ze pojavila frekvenco povecamo za 1
					if (map.containsKey(s)) {
						map.put(s, map.get(s) + 1);
					} else {
						//sicer frekvenco nastavimo na 1
						map.put(s, 1);
					}
				}
			}
			//bralnik zapremo
			sc.close();
			//v seznam slovarjov dodamo ravnokar zgrajeni slovar
			sezDoc.add(map);
			//dodamo dokument v ArrayList vseh dokumentov
			dokumenti.add(file.getName());
		}
		double[][] frekMat = new double[besede.size()][sezDoc.size()];

		//zgradimo matriko A na podlagi dobljenih slovarjev
		int i = 0;
		//iteriramo skozi vse besede->besede predstavljajo vrstice
		for (String key : besede.keySet()) {
			//za i-to besedo preverimo, ce je v j-tem dokumentu
			for (int j = 0; j < sezDoc.size(); j++) {
				//dobimo ustrezen (j-ti) slovar iz ArrayLista, ki vsebuje vse slovarja
				LinkedHashMap<String, Integer> hm = sezDoc.get(j);
				if (sezDoc.get(j).containsKey(key)) {
					//nastavimo vrednost matrike na zabelezeno frekvenco besede
					frekMat[i][j] = hm.get(key);
				}
			}
			//premik na drugo besedo (pisanje v drugo vrstico)
			i++;
		}
		//vrnemo zgrajeno matriko
		return new DoubleMatrix(frekMat);
	}

	//matrika vrne k-vrednosti iz podanega svd razcepa
	public DoubleMatrix[] approximationMat(DoubleMatrix U, DoubleMatrix S, DoubleMatrix V, int k) {
		//iz knjiznice jblas dobimo v matriko S le singularne vrednosti
		//zato jo razsirimo z nulami, saj jih potrebujemo za mnozenje matrik
		//sicer se dimenzije nebi ujemale
		double matrikaS[][] = new double[k][k];
		for (int i = 0; i < k; i++) {
			matrikaS[i][i] = S.get(i, 0);
		}
		//vrnemo zahtevanih k-vrednosti iz obstojecega svd razcepa
		return new DoubleMatrix[] { U.get(row(U.rows), col(k)), new DoubleMatrix(matrikaS),
				V.get(row(k), col(V.getColumns())) };
	}
	//metoda s pomocjo katere dobimo n-vrstic matrike
	public int[] row(int n) {
		int a[] = new int[n];
		for (int i = 0; i < n; i++) {
			a[i] = i;
		}
		return a;
	}
	//metoda vrne k-vrednosti iz matrike s pomocjo racunanja Frobeniusove norme
	public int automatedK(DoubleMatrix m, DoubleMatrix[] svd) {
		//izracun norme zacetne matrike
		double org = m.norm2();
		//racunanje zacnemo pri k=2
		int k = 2;
		//vzameo k-vrednosti iz matrike
		DoubleMatrix[] mat = approximationMat(svd[0], svd[1], svd[2].transpose(), k);
		//izracunanmo zacetno aproksimirano matriko
		DoubleMatrix n1 = mat[0].mmul(mat[1]).mmul(mat[2]);
		//izracun zacetnega odstopanja
		double zacetni = Math.abs(n1.norm2() - org);
		//si ga shranimo v tmp
		double tmp = zacetni;
		//zacenmo aproksimacijo pri k=10 in povecujemo k za 10
		for (int j1 = 10; j1 < m.columns; j1 += 10) {
			//izracunamo aproksimirano matriko pri izbranem k-ju oz. j1
			mat = approximationMat(svd[0], svd[1], svd[2].transpose(), j1);
			n1 = mat[0].mmul(mat[1]).mmul(mat[2]);
			//izracun odstopanja
			double ods = Math.abs(org - n1.norm2());
			//ko vrednosti padejo pod izbrano mejo racnananje prenehamo
			if (Math.abs(ods - tmp) < zacetni * 0.05) {
				k = j1;
				break;
			}
			tmp = ods;
		}
		//vrnemo izracunan k
		return k;
	}

	//metoda obdela nov dokument in klice metodo, ki izracuna nov SVD aproksimiran na podlagi obstojecega
	public DoubleMatrix[] adddoc(DoubleMatrix[] existingSVD, File novFajl, DoubleMatrix originalMatrix)
			throws FileNotFoundException {
		//pripravimo slovar za belezenje besed in njihovih frekvenc
		LinkedHashMap<String, Integer> map = new LinkedHashMap<String, Integer>();
		Scanner sc = new Scanner(novFajl);
		//sprehod skozi vse besede
		while (sc.hasNext()) {
			//spremenimo besedo v male crke in odrezemo vse znake ki niso crke
			String s = sc.next().toLowerCase().replaceAll("[^a-zA-Z]", "");
			//preverimo ce se kaj od te besede ostane
			if (s.length() >= 1) {
				//dodanmo v globalen slovar besede in frekvence
				//ce se je beseda ze pojavila frekvenco povecamo za 1
				if (besede.containsKey(s)) {
					besede.put(s, besede.get(s) + 1);
				} else {
					//sicer frekvenco nastavimo na 1
					besede.put(s, 1);
				}
				//enako poskrbimo se za slovar, ki ga trenutno gradimo
				if (map.containsKey(s)) {
					map.put(s, map.get(s) + 1);
				} else {
					map.put(s, 1);
				}
			}
		}
		//scanner zapremo
		sc.close();
		//pripravimo si 2d polje realnih stevil za novo sprocesiran dokument
		double frekMat[] = new double[besede.size()];
		//i-to besedo zabelezimo v i-to vrstico matrike
		int i = 0;
		for (String k : besede.keySet()) {
			//ce se je beseda pojavila ji nastavimo na novo zabelezeno frekvenco
			if (map.containsKey(k)) {
				frekMat[i] = map.get(k);
			}
			//premik na naslednjo vrstico matrike frekMat
			i++;
		}
		//zabelezimo si prvotno stevilo besed
		int tmpSize = originalMatrix.rows;
		//ce ni novih besed, pripnemo le nov stolpec
		if (frekMat.length == originalMatrix.rows) {
			originalMatrix = originalMatrix.concatHorizontally(originalMatrix, new DoubleMatrix(frekMat));
		} else {
			//sicer pripnemo ustrezno stevil 0, saj gre za nove besede
			//in jih pripnemo v matriko ter dodamo �e nov stolpec oz. nov dokument
			double nule[][] = new double[besede.size() - originalMatrix.rows][originalMatrix.columns];
			originalMatrix = originalMatrix.concatVertically(originalMatrix, new DoubleMatrix(nule));
			originalMatrix = originalMatrix.concatHorizontally(originalMatrix, new DoubleMatrix(frekMat));
		}
		//nadomestimo vrednosti frekvenc z globalno in lokalno mero
		originalMatrix=approx(originalMatrix,true);
		modifyMatrix=originalMatrix;
		//ce so nove besede dodane
		if (tmpSize != originalMatrix.rows) {
			//jih v matriko ustrezno dodamo
			int ustrezneVrstice[] = new int[besede.size() - tmpSize];

			for (int j = tmpSize, ind = 0; ind < ustrezneVrstice.length; j++, ind++) {
				ustrezneVrstice[ind] = j;
			}
			//klic funkcije, ki na podlagi obstojecega svd razcepa aproksimira novega
			return aprox(existingSVD, originalMatrix.get(row(tmpSize), originalMatrix.columns - 1),
					originalMatrix.get(ustrezneVrstice, col(originalMatrix.columns)));
		}
		//sicer novih besed ni dodanih, torej klicemo metodo z null
		return aprox(existingSVD, originalMatrix.get(row(tmpSize), originalMatrix.columns - 1), null);
	}
	
	//metoda na podlagi obstojecega svd razcepa izracuna novega
	public DoubleMatrix[] aprox(DoubleMatrix[] existingSVD, DoubleMatrix newDocument, DoubleMatrix newWords) {

		//donimo najprej stare vrednosti
		DoubleMatrix oldU = existingSVD[0];
		DoubleMatrix oldS = existingSVD[1];
		DoubleMatrix oldV = existingSVD[2].transpose();
		//na podlagi enacb izracunamo nove elemente matrike V
		DoubleMatrix novStolpec = Solve.pinv(oldS).mmul(oldU.transpose()).mmul(newDocument);
		//dobljene vrednosti pripnemo v obstojeci V
		DoubleMatrix novV = oldV.concatVertically(oldV, novStolpec.transpose());
		//ce ni novih besed potem ni potreben nov izracun matrike U
		if (newWords != null) {
			//na podlagi enacb izracunamo nove elemente matrike U
			DoubleMatrix novaVrstica = newWords.mmul(novV).mmul(Solve.pinv(oldS));
			//dobljene vrednosti pripnemo v matriko U
			DoubleMatrix novU = oldU.concatVertically(oldU, novaVrstica);
			//rezultate vrnemo
			return new DoubleMatrix[] { novU, oldS, novV, };
		}
		//izracunan svd vrnemo
		return new DoubleMatrix[] { oldU, oldS, novV };
	}
	//metoda vrne ustrezno polje n-tih stolpcev
	//v pomoc pri dobivanju ustreznih vrednosti iz matrik
	public int[] col(int n) {
		int a[] = new int[n];
		for (int i = 0; i < n; i++) {
			a[i] = i;
		}
		return a;
	}

}