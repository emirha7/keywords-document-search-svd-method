import java.awt.Desktop;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import org.jblas.DoubleMatrix;
import javafx.application.Application;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.Slider;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

public class GUI extends Application {
//Priprava graficnih elementov
	@FXML
	private TextField pickfolder;

	@FXML
	private Button confolder;

	@FXML
	private CheckBox svdfull;

	@FXML
	private CheckBox svdaut;

	@FXML
	private CheckBox svdcust;

	@FXML
	private Slider svdslider;

	@FXML
	private CheckBox searaut;

	@FXML
	private CheckBox searcust;

	@FXML
	private Slider searslider;

	@FXML
	private TextField query;

	@FXML
	private Button run;

	@FXML
	private ListView<String> listdoc;

	@FXML
	private ListView<String> listres;

	@FXML
	private TextArea runtime;

	@FXML
	private TextArea svdk;

	@FXML
	private TextArea wordc;

	@FXML
	private TextArea docc;

	@FXML
	private TextArea searprec;

	@FXML
	private TextField svdval;

	@FXML
	private TextField searval;

	@FXML
	private TextArea status;

	@FXML
	private Button runsvd;

	@FXML
	private Button addDoc;

	public static void main(String[] args) {
		launch(args);
	}

	static Stage primaryST;
	
	
//Nit graficnega vmesnika
	@Override
	public void start(Stage primary) throws Exception {

		primaryST = primary;
		primary.setTitle("LSI");
		try {
			FXMLLoader loader = new FXMLLoader(GUI.class.getResource("Layout.fxml"));
			VBox vBox = (VBox) loader.load();
			Scene scene = new Scene(vBox);
			primary.setScene(scene);
			primary.show();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}
	//Nit za izracun SVD razcepa
	class CalcThread implements Runnable {

		@Override
		public void run() {
			//inicializacije nove matrike
			frequencyMatrix = new FrequencyMatrix();
			
			try {
				//izracun frekvencne matrike
				frekm = frequencyMatrix.calcMatrix(dir);
			} catch (FileNotFoundException e2) {
				e2.printStackTrace();
			}
			//popravek frekvencne matrike z lokalno in globalno mero
			DoubleMatrix afrekm = frequencyMatrix.approx(frekm,false);
			start_time = System.nanoTime();
			//izracun SVD razcepa
			svd = frequencyMatrix.calcSVD(afrekm);
			//branje uporabnikove izbire za stevilo k
			if (arraySVD[0].isSelected()) {
				//izbira mo�nosti "Full"
				k = dir.listFiles().length;
			} else if (arraySVD[1].isSelected()) {
				//mo�nost "Automatic"
				k = frequencyMatrix.automatedK(afrekm, svd);
			} else if (arraySVD[2].isSelected()) {
				//poljubno stevilo k
				k = Integer.parseInt(svdval.getText());
			}
			//rezanje SVD razcepa glede na k
			svd = frequencyMatrix.approximationMat(svd[0], svd[1], svd[2].transpose(), k);
			rtime = System.nanoTime() - start_time;
			//posodobitev graficnega umesnika
			runtime.setText("" + rtime);
			svdk.setText("" + k);
			arraySEAR[0].setDisable(false);
			arraySEAR[1].setDisable(false);
			status.setStyle("-fx-text-fill: #d8c70f");
			status.setText("STATUS: SVD CALCULATED");

			addDoc.setDisable(false);
			
		}
		
	}
    //priprava staticnih spremenljivk
	static File dir;
	static File file;
	static boolean checkSVD = false;
	static boolean checkSEAR = false;
	static CheckBox[] arraySVD;
	static CheckBox[] arraySEAR;
	static int doccount = 0;
	static Label label = new Label("0");
	static FrequencyMatrix frequencyMatrix;
	static volatile ArrayList<String> result;
	static long start_time = 0;
	static volatile int k = 1;
	static volatile double cos = 0.5;
	static volatile long rtime = 0;
	static volatile String search;
	static volatile DoubleMatrix[] svd;
	static DoubleMatrix frekm;
	static DoubleMatrix afrekm;
    
	//pomozna funkcija za ispis matrike
	static void out(double a[][]) {
		for (int i = 0; i < a.length; i++) {
			for (int j = 0; j < a[i].length; j++) {
				System.out.print(a[i][j] + " ");
			}
			System.out.println();
		}
		System.out.println();
	}
	
	
	//Inicializacija graficnega umesnika
	@FXML
	private void initialize() throws Exception {
		runsvd.setDisable(true);
		run.setDisable(true);
		query.setDisable(true);
		svdval.setDisable(true);
		searval.setDisable(true);
		svdslider.setDisable(true);
		svdslider.valueProperty().addListener(e -> {
			svdval.setText("" + (int) svdslider.getValue());
		});
		searslider.setDisable(true);
		searslider.valueProperty().addListener(e -> {
			searval.setText("" + (double) searslider.getValue());
		});
		searslider.setMin(0);
		searslider.setMax(1);
		arraySVD = new CheckBox[3];
		arraySVD[0] = svdfull;
		arraySVD[1] = svdaut;
		arraySVD[2] = svdcust;
		arraySEAR = new CheckBox[2];
		arraySEAR[0] = searaut;
		arraySEAR[1] = searcust;
		arraySVD[0].setDisable(true);
		arraySVD[1].setDisable(true);
		arraySVD[2].setDisable(true);
		arraySEAR[0].setDisable(true);
		arraySEAR[1].setDisable(true);
		addDoc.setDisable(true);
		status.setStyle("-fx-text-fill: #a01d1b");
		status.setText("STATUS: SEARCH UNAVAILABLE");
		System.out.println("Running");
		//funkcionalnost elementov graficnega umesnika
		pickfolder.setOnMouseClicked((event) -> {
			//izbira dokumentov
			pickfolder.clear();
			DirectoryChooser dc = new DirectoryChooser();
			dir = dc.showDialog(primaryST);
			pickfolder.appendText(dir.getAbsolutePath());
		});

		confolder.setOnMouseClicked((event) -> {
			//potrditev izbire in prikaz seznama dokumentov
			doccount = 0;
			if (dir != null) {
				try {
					listdoc.getItems().clear();
					for (File file : dir.listFiles()) {
						listdoc.getItems().add(file.getName());
						doccount++;
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
				svdslider.setMin(1);
				svdslider.setMax(doccount);
				arraySVD[0].setDisable(false);
				arraySVD[1].setDisable(false);
				arraySVD[2].setDisable(false);
				runsvd.setDisable(false);
			}
		});

		listdoc.setOnMouseClicked((event) -> {
			//izbira dokumenta v seznamu "Documents"
			String fil = listdoc.getSelectionModel().getSelectedItem();
			try {
				Desktop.getDesktop().open(new File(dir.getAbsolutePath() + "/" + fil));
			} catch (IOException e) {
				e.printStackTrace();
			}
		});

		listres.setOnMouseClicked((event) -> {
			//izbira dokumenta v seznamu "Results" 
			String fil = listres.getSelectionModel().getSelectedItem();
			try {
				Desktop.getDesktop().open(new File(dir.getAbsolutePath() + "/" + fil));
			} catch (IOException e) {
				e.printStackTrace();
			}
		});
		
		svdfull.setOnMouseClicked((event) -> {
			//mo�nost "Full" za izbiro stevila k
			if (!checkSVD) {
				arraySVD[1].setDisable(true);
				arraySVD[2].setDisable(true);
				checkSVD = true;
				runsvd.setDisable(false);
				status.setStyle("-fx-text-fill: #59d63e");
				status.setText("STATUS: SVD AVAILABLE");
			} else {
				arraySVD[1].setDisable(false);
				arraySVD[2].setDisable(false);
				checkSVD = false;
				runsvd.setDisable(true);
				status.setStyle("-fx-text-fill: #d8c70f");
				status.setText("STATUS: PARAMETERS NEEDED");
			}
		});
		svdaut.setOnMouseClicked((event) -> {
			//mo�nost "Automatic" za izbiro stevila k
			if (!checkSVD) {
				arraySVD[0].setDisable(true);
				arraySVD[2].setDisable(true);
				checkSVD = true;
				runsvd.setDisable(false);
				status.setStyle("-fx-text-fill: #59d63e");
				status.setText("STATUS: SVD AVAILABLE");
			} else {
				arraySVD[0].setDisable(false);
				arraySVD[2].setDisable(false);
				checkSVD = false;
				runsvd.setDisable(true);
				status.setStyle("-fx-text-fill: #d8c70f");
				status.setText("STATUS: PARAMETERS NEEDED");
			}
		});
		svdcust.setOnMouseClicked((event) -> {
			//mo�nost "Custom" za izbiro stevila k
			if (!checkSVD) {
				arraySVD[0].setDisable(true);
				arraySVD[1].setDisable(true);
				svdslider.setDisable(false);
				svdval.setDisable(false);
				checkSVD = true;
				runsvd.setDisable(false);
				status.setStyle("-fx-text-fill: #59d63e");
				status.setText("STATUS: SVD AVAILABLE");
			} else {
				svdslider.setDisable(true);
				arraySVD[0].setDisable(false);
				arraySVD[1].setDisable(false);
				svdval.setDisable(true);
				checkSVD = false;
				runsvd.setDisable(true);
				status.setStyle("-fx-text-fill: #d8c70f");
				status.setText("STATUS: PARAMETERS NEEDED");
			}
		});

		searaut.setOnMouseClicked((event) -> {
			//mo�nost "Automatic" za izbiro natancnosti iskanja
			if (!checkSEAR) {
				arraySEAR[1].setDisable(true);
				checkSEAR = true;
				run.setDisable(false);
				query.setDisable(false);
				status.setStyle("-fx-text-fill: #59d63e");
				status.setText("STATUS: SEARCH AVAILABLE");
			} else {
				arraySEAR[1].setDisable(false);
				checkSEAR = false;
				run.setDisable(true);
				query.setDisable(true);
				status.setStyle("-fx-text-fill: #d8c70f");
				status.setText("STATUS: PARAMETERS NEEDED");
			}
		});

		searcust.setOnMouseClicked((event) -> {
			//mo�nost "Custom" za izbiro natancnosti iskanja
			if (!checkSEAR) {
				arraySEAR[0].setDisable(true);
				searval.setDisable(false);
				searslider.setDisable(false);
				checkSEAR = true;
				run.setDisable(false);
				query.setDisable(false);
				status.setStyle("-fx-text-fill: #59d63e");
				status.setText("STATUS: SEARCH AVAILABLE");
			} else {
				searslider.setDisable(true);
				arraySEAR[0].setDisable(false);
				searval.setDisable(true);
				checkSEAR = false;
				run.setDisable(true);
				query.setDisable(true);
				status.setStyle("-fx-text-fill: #d8c70f");
				status.setText("STATUS: PARAMETERS NEEDED");
			}
		});

		run.setOnMouseClicked((event) -> {
			//iskanje po zbirki dokumentov
			result = new ArrayList<String>();
			
			search = query.getText();
			if (arraySEAR[1].isSelected()) {
				cos = Double.parseDouble(searval.getText());
			}
			//klic metode za primerjanje iskalnega vektorja z vektorji dokumentov
			result = frequencyMatrix.search(afrekm, search, k, svd, cos);

			System.out.println(k);
			wordc.setText("" + svd[0].rows);
			docc.setText("" + dir.listFiles().length);
			searprec.setText("" + cos);
			System.out.println("cos: " + cos + " k: " + k);
			System.out.println(result);
			listres.getItems().clear();
			for (String s : result) {
				listres.getItems().add(s);
			}
			status.setStyle("-fx-text-fill: #0c0c0c");
			status.setText("STATUS: SEARCH COMPLETE");

		});

		runsvd.setOnMouseClicked((event) -> {
			//izracun SVD razcepa
			status.setStyle("-fx-text-fill: #3b80ef");
			status.setText("STATUS: SVD IN PROGRESS");
			//zagon niti za izracun SVD razcepa
			Thread thread = new Thread(new CalcThread());
			thread.start();
		});

		addDoc.setOnMouseClicked((event) -> {
			//dodajanje dokumenta
			FileChooser fc = new FileChooser();
			file = fc.showOpenDialog(primaryST);		
			
			try {
				System.out.println(file.getAbsolutePath());
				//klic metode za dodajanje dokumenta
				svd = frequencyMatrix.adddoc(svd, file, frekm);
				frekm = frequencyMatrix.modifyMatrix;
				svd[2]=svd[2].transpose();
			} catch (FileNotFoundException e1) {
				e1.printStackTrace();
			}
			frequencyMatrix.dokumenti.add(file.getName());
			listdoc.getItems().add(file.getName());
		});
	}

}
